#!/usr/bin/python3
# -*- coding: utf-8 -*-
from timeit import repeat
from numbers import Number
from functools import reduce
from numpy import product


class Three:
    def __init__(self):
        pass

    def three(*args, **kwargs):
        nums = (x for x in list(args)+list(kwargs.values()) if isinstance(x, (int, float, complex)))
        return reduce(lambda x, y: x * y, nums, 1)


class Two:
    def __init__(self):
        pass

    def two(*args, **kwargs):
        nums = (x for x in list(args) + list(kwargs.values()) if isinstance(x, (int, float, complex)))
        ans = 1
        for num in nums:
            ans *= num
        return ans


class One:
    def __init__(self):
        pass

    def one(*args, **kwargs):
        argys = args + tuple(kwargs.values())
        x = [n for n in argys if isinstance(n, (int, float, complex))]
        ans = 1
        for num in x:
            ans *= num
        return ans


if __name__ == '__main__':
    print(min(repeat("One().one(1,2,3,4,5,6,7,8,9,'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m')", number=10000, globals=globals(), repeat=10)))
    # 0.10724777198629454

    print(min(repeat("Two().two(1,2,3,4,5,6,7,8,9,'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m')", number=10000, globals=globals(), repeat=10)))
    # 11455935699632391

    print(min(repeat("Three().three(1,2,3,4,5,6,7,8,9,'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m')", number=10000, globals=globals(), repeat=10)))
    # 12201370199909434
