#!/usr/bin/python3
# -*- coding: utf-8 -*-
from timeit import repeat
from math import sqrt


class NotLambda:
    # def __init__(self):
        # pass

    def a(self, num):
        selfB = self.b
        for __ in range(10000000):
            selfB(num)

    def b(self, num):
        return sqrt(num)


class Lambda:
    # def __init__(self):
        # pass

    def a(self, num):
        selfA = self.a
        for __ in range(10000000):
            lambda num: sqrt(num)


if __name__ == '__main__':
    print(min(repeat('NotLambda().a(64)', number=1, globals=globals(), repeat=10)))
    # 1.311144029998104

    print(min(repeat('Lambda().a(64)', number=1, globals=globals(), repeat=10)))
    # 0.6493413540010806
