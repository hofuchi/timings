#!/usr/bin/python3
# -*- coding: utf-8 -*-
from timeit import repeat
from tempfile import SpooledTemporaryFile
from array import array
from io import BytesIO


def forLoop():
    array = []
    for num in range(1000000):
        array.append(num)


def listComprehension():
    array = [num for num in range(1000000)]


def memoryFileListComprehension():
    fp = SpooledTemporaryFile()
    fp.write(array('I', [num for num in range(1000000)]).tostring())
    fp.close()


def streamFileListComprehension():
    f = BytesIO()
    f.write(array('I', [num for num in range(1000000)]).tostring())
    f.close()


if __name__ == '__main__':
    print(min(repeat('forLoop()', number=1, globals=globals(), repeat=10)))
    # 0.08336105499984114

    print(min(repeat('listComprehension()', number=1, globals=globals(), repeat=10)))
    # 0.040542392001952976

    print(min(repeat('memoryFileListComprehension()', number=1, globals=globals(), repeat=10)))
    # 0.05214879399864003

    print(min(repeat('streamFileListComprehension()', number=1, globals=globals(), repeat=10)))
    # 0.05029467200074578
